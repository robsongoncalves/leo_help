#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void mostraMenu(){
  printf("##### MENU ##### \n");
  printf("Este programa tem como finalidade a Verificação e a Validação de Expressões Proposicionais. \n\n");
  printf("Gostaria de saber quais preposições podem ser usadas? S ou N \n");
  printf("Para encerrar digite x \n");
  printf("################ \n");
}

void mostraProposicoes(){
  printf("Use ~ para representar a negação ().\n");
  printf("Use & para representar a conjunção (∧).\n");
  printf("Use | para representar a disjunção (∨).\n");
  printf("Use > para representar a implicação (→)\n");
  printf("Use <> para representar a bi-implicação (↔).\n\n\n\n");
}

//  char testaProposicao(char * proposicao);

void testaProposicao(char *proposicao){
  char * opcoes[] = {'~','<>','>'};
  // int i, j, tamanhoProposicao, tamanhoOpcoes;
  printf("Lendo Proposicao: %s \n", proposicao);
  // tamanhoProposicao = strlen(proposicao);
  tamanhoOpcoes = strlen(opcoes);
  // printf("Tamanhos %i \n", tamanhoOpcoes);
  // printf("Tamanhos %d \n", tamanhoProposicao);
  // if(strchr(opcoes, *proposicao)){
  //   printf("Proposicao Valida! \n");
  // }
  // else{
  //   printf("Proposicao Invalida! \n");
  // }
  // return * proposicao;


  // for(j=0; j<tamanhoOpcoes ; j++){
  //   if (opcoes[j] == proposicao){
  //     printf("Proposicao Valida! \n");
  //   }
  //   else{
  //     printf("Proposicao Invalida! \n");
  //   }
  // }

  for(int j=0;n<tamanhoOpcoes;j++){
    if(opcoes[j] == proposicao){
      return true;
    }
   return false;
  }
}



char lerProposicao(){
  char * proposicao[61];
  // char result;

  printf("Favor inserir o proposição para que seja feita a validação: \n");
  scanf("%s", &proposicao);
  // gets(proposicao);
  printf("Proposicao: %s \n", proposicao);
  testaProposicao(proposicao);
  // printf("Test ok\n");

  // return result;
}




























void main () {

  char resposta;
  do {
    mostraMenu();
    resposta = getchar();
    getchar(); // discard line feed    
    // resposta = mostraMenu(resposta);

    // printf("A resposta digitada é: \n");
    if(resposta == 'S' || resposta == 's') {
      mostraProposicoes();
    }

    else if (resposta == 'N' || resposta == 'n') {
      lerProposicao();
    }

    if(resposta != 'x'){
      printf(" Limpando o console em 5 segundos... \n");
      sleep(5); 
      system("clear"); 
    }
  } while (resposta != 'x');

  // exit(0);
}